const plugin = require('tailwindcss/plugin')
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}", 
  ],
  theme: {
    extend: {
      fontFamily: {
        interMedium: ['INTER-MEDIUM',],
        interRegular: [ 'INTER-REGULAR'],
        interSemiBold: [ 'INTER-SEMIBOLD'],
        interBold: ['INTER-BOLD'],
        sans: [
          'Rubik',
          'system-ui',
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          '"Noto Sans"',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
          '"Noto Color Emoji"',
        ]
      },
      colors: {
        'bongasandbox-simulator-bg': '#F9FAFB',
        'bongasandbox-blue': '#78B9E4',
        'bongasandbox-black': '#101828',
        'bongasandbox-grey-label': '#475467',
        'bongasandbox-grey': '#667085',
        'bongasandbox-blue-hover': '#3182ce',
        'bongasandbox-grey-overlay': '#F8FAFC',
        'bongasandbox-modal-overlay' : '#344054',
        'bongasandbox-card-bg': 'rgba(178, 202, 207, 0.08)',
        'bongasandbox-card-bg-hover': 'rgba(178, 202, 207, 0.2)',
        'bongasandboc-icon-overlay': 'rgba(178, 202, 207, 0.08)',
        'bongasandbox-tally-overlay' : 'rgba(178, 202, 207, 0.24)'
      }
    },
  },
  plugins: [
    // ...
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),

    plugin(function ({ addBase, addComponents, addUtilities, theme }) {
      addBase({
        a: {
          color: '#78B9E4',
        },
        'a:hover': {
          color: '#3182ce',
        },
      })
    })
  ],
}
