import { useRoutes } from "react-router-dom"
import LandingPage from "./pages"
import PageNotFound from "./pages/404"
import ForgotPassword from "./pages/auth/ForgotPassword"
import Login from "./pages/auth/Login"
import Signup from "./pages/auth/Signup"
import Api from "./pages/Api"
import Apps from "./pages/apps"
import CreateApp from "./pages/apps/CreateApp"
import Simulator from "./pages/simulator"

const CustomRoutes = () => {
    const routes = useRoutes([
        {
            path: "/",
            element: <LandingPage/>,
        },
        {
            path: '/auth',
            element: <Login/>,
            children: [
                {
                    path: 'signup',
                    element: <Signup/>
                },
                {
                    path: 'forgotpassword',
                    element: <ForgotPassword/>
                }
            ]
        },
        {
            path: '/api',
            element: <Api/>
        },
        {
            path: '/apps',
            element: <Apps/>,
            children: [
                {
                    path: 'createapp',
                    element: <CreateApp/>
                }
            ]
        },
        {
            path: '/simulator',
            element: <Simulator/>
        },
        {
            path: '*',
            element: <PageNotFound/>
        }
    ])


    return routes
}

export default CustomRoutes;