import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ToastContainer } from "react-toastify";
import { BrowserRouter } from "react-router-dom";
import CustomRoutes from "./Routes";

const queryClient = new QueryClient();

function App() {
	return (
		<QueryClientProvider client={queryClient}>
			<ToastContainer limit={1} />
			<BrowserRouter>
				<CustomRoutes />
			</BrowserRouter>
		</QueryClientProvider>
	);
}

export default App;
