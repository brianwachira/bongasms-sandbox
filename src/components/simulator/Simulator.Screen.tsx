import IphoneFrame from "../../assets/images/iphone-frame2.png";
import {
	WifiIcon,
	PhoneIcon,
	ChatBubbleBottomCenterTextIcon,
	HomeIcon,
} from "@heroicons/react/24/outline";
import { Battery100Icon } from "@heroicons/react/24/outline";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

interface ISimulatorScreenProps {
	children: React.ReactNode;
}

// Simulator screen.
const SimulatorScreen = (props: ISimulatorScreenProps) => {
	const { children } = props;

	const [dateAndTime, setDateAndTime] = useState(new Date());

	useEffect(() => {
		const timer = setInterval(() => {
			// Creates an interval which will update the current date every second.
			// This will trigger a rerender on every component that uses the useClock hook.
			setDateAndTime(new Date());
		}, 1000);
		return () => {
			// Return a funtion to clear the timer so that it will stop being called on unmount.
			clearInterval(timer);
		};
	}, []);

	const time = dateAndTime.toLocaleString("en-US", {
		hour: "numeric",
		minute: "numeric",
		hour12: true,
	});

	return (
		<div className="flex justify-end w-72 h-[40rem] -mt-32 relative">
			<img
				src={IphoneFrame}
				alt="Iphone frame"
				className="absolute w-full bottom-0 z-0"
			/>
			<div className="flex flex-col mx-auto h-5/6 w-[80%] top-20 relative">
				<div className="flex w-full justify-between items-start">
					<p className=" font-interRegular text-xs">{time}</p>
					<div className="inline-flex">
						<WifiIcon className=" w-3 h-3 mr-2" />
						<Battery100Icon className="w-3 h-3" />
					</div>
				</div>
				{children}
				<div className="mt-auto w-full flex justify-between">
					<Link to={''}>
						<div className="h-12 w-12 rounded-lg shadow-lg bg-bongasandbox-card-bg flex justify-center items-center cursor-pointer">
							<HomeIcon className="h-8 text-gray-600 pointer-events-none" />
						</div>
					</Link>
					<Link to={'?dial'}>
						<div className="h-12 w-12 rounded-lg shadow-lg bg-bongasandbox-card-bg flex justify-center items-center cursor-pointer">
							<PhoneIcon className="h-8 text-gray-600 pointer-events-none" />
						</div>
					</Link>
					<Link to={'?chat'}>
						<div className="h-12 w-12 rounded-lg shadow-lg bg-bongasandbox-card-bg flex justify-center items-center cursor-pointer">
							<ChatBubbleBottomCenterTextIcon className="h-8 text-gray-600 pointer-events-none" />
						</div>
					</Link>
				</div>
			</div>
		</div>
	);
};

SimulatorScreen.displayName = "SimulatorScreen";

// Exported components can now be used as <SimulatorScreen>
export default Object.assign(SimulatorScreen);
