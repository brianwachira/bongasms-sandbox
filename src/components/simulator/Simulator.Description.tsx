interface ISimulatorDescriptionProps {
	children: React.ReactNode;
}

// Simulator description.
const SimulatorDescription = (props: ISimulatorDescriptionProps) => (
	<div className="hidden lg:flex flex-row">
		<div className="basis-1/5">
			<div className="mx-auto">
				<div className=" mb-12 w-24 h-24 rounded-full border-bongasandbox-blue border flex justify-center items-center">
					<span className=" text-bongasandbox-blue font-interBold text-5xl">
						⚫
					</span>
				</div>
				<div className="border border-bongasandbox-blue h-56 w-0 mx-auto" />
			</div>
		</div>
		<div className="mt-24 w-96"><div>{props.children}</div></div>
	</div>
);

// Simulator description heading
const SimulatorDescriptionHeading = (props: ISimulatorDescriptionProps) => (
	<h3 className="font-interBold text-5xl text-bongasandbox-blue mb-8">{props.children}</h3>
);

// Simulator description content.
const SimulatorDescriptionContent = (props: ISimulatorDescriptionProps) => (
	<p className="font-interRegular text-2xl text-[#02173F]">{props.children}</p>
);

SimulatorDescription.displayName = "SimulatorDescription";

// Exported components can now be used as <SimulatorDescription> and <SimulatorDescription.Heading> and <SimulatorDescription.Content>
export default Object.assign(SimulatorDescription, {
	Heading: SimulatorDescriptionHeading,
	Content: SimulatorDescriptionContent,
});
