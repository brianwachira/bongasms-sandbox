import SimulatorDescription from "./Simulator.Description";
import SimulatorScreen from "./Simulator.Screen";

interface ISimulatorProps {
    children: React.ReactNode
}

const Simulator = (props : ISimulatorProps) => (
    <main className=" bg-bongasandbox-simulator-bg py-12 px-24 rounded-lg">
        <div className="flex justify-around">
            {props.children}
        </div>
    </main>
)

Simulator.displayName = 'Simulator';

// Exported components now can be used as <Simulator> and <Simulator.Description> and <Simulator.Screen>
export default Object.assign(Simulator, {Description: SimulatorDescription, Screen: SimulatorScreen})

