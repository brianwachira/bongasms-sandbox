import { ReactComponent as BongaIconLight } from '../assets/icons/bonga-icon-light.svg' 
import { SunIcon, MoonIcon,Bars2Icon, XMarkIcon} from "@heroicons/react/24/outline"
import { Popover } from "@headlessui/react"
import { Link, NavLink } from "react-router-dom"

const navigation = [
    {
        title: 'Home',
        url: '/'
    },
    {
        title: 'My Apps',
        url: '/apps'
    },
	{
		title: "Simulator",
		url: "/simulator",
	},
    {
        title: 'API',
        url: '/api'
    },
    {
        title: 'FAQ',
        url: '/faq'
    }
]

//dark:text-white dark:hover:text-bongadocs-primary
const NavBarLoggedIn = () => {
    return (
        <Popover as="main">
            {({ open }) => (
                <>
                    <header
                        className={`${
                            open ? "fixed inset-0 z-40 overflow-y-auto" : ""
                        }  mx-auto  dark:bg-inherit xl:bg-white shadow-sm border-b-gray-100  lg:overflow-y-visible`}
                    >
                        <div className="mx-auto xl:w-[86vw] space-y-1 px-5 py-2">
                            <div className="relative flex justify-between  items-center">
                                <div className="flex md:inset-y-0 items-center">
                                    <div className="flex-shrink-0 flex items-center">
                                        <Link to={"/"}>
                                            <div className="flex infline-flex">
                                                <BongaIconLight className="mr-2 w-28" />
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                                <div className="items-center justify-between hidden w-full lg:flex md:w-auto -mr-10">
                                    <div className="flex flex-row">
                                        {navigation.map((item) => (
                                            <NavLink
                                                key={item.title}
                                                to={item.url}
                                                className="text-base font-interSemiBold mr-8 text-bongasandbox-grey-label hover:text-bongasandbox-blue-hover "
                                            >
                                                {item.title}
                                            </NavLink>
                                        ))}
                                    </div>
                                </div>
                                <div className="flex items-center md:absolute md:right-0 md:inset-y-0 lg:hidden">
                                    {/* mobile menu button */}
                                    <Popover.Button
                                        className={
                                            "-mx-2 rounded-md p-2 inline-flex items-center justify-center border-2 border-bongadocs-primary  text-bongadocs-primary hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-bongadocs-primary"
                                        }
                                    >
                                        <span className="sr-only">Open Menu</span>
                                        {!open ? (
                                            <Bars2Icon
                                                className="block h-6 w-6"
                                                aria-hidden="true"
                                            />
                                        ) : (
                                            <XMarkIcon
                                                className="block h-6 w-6"
                                                aria-hidden="true"
                                            />
                                        )}
                                    </Popover.Button>
                                </div>
                                <div className="hidden min-w-0  md:px-8 lg:px-0 lg:flex items-center justify-end">
                                    <Link to={"/auth"}>
                                        <p className="text-base font-interSemiBold text-bongasandbox-grey-label hover:text-bongasandbox-blue-hover">
                                            logout
                                        </p>
                                    </Link>
                                    <MoonIcon className="ml-6 h-5 w-5 inline-flex cursor-pointer text-bongadocs-text" />
                                </div>
                            </div>
                        </div>
                        <Popover.Panel as="nav" className="bg-white lg:hidden" aria-label="Global">
                            <div className="max-w-3xl mx-auto px-4 pt-4 pb-3 space-y-1 sm:px-4">
                                {navigation.map((item) => (
                                    <NavLink
                                        key={item.title}
                                        to={item.url}
                                        className={`hover:bg-gray-50 hover:text-gray-900 block rounded-md py-2 px-3 text-bongadocs-primary font-medium`}
                                    >
                                        {item.title}
                                    </NavLink>
                                ))}
                            </div>
                        </Popover.Panel>
                    </header>
                </>
            )}
        </Popover>
    )
}

export default NavBarLoggedIn