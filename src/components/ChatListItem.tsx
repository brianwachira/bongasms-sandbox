import { ChevronRightIcon } from "@heroicons/react/24/outline";

import { UserCircleIcon } from "@heroicons/react/24/solid";
import { Link } from "react-router-dom";

interface IChatListItem {
    id: string;
    title: string,
    time: string,
    description: string

}

const ChatListItem = (props : IChatListItem) => {

    const {id, title, time, description} = props

    return (
        <Link to={`?${id}`} className="flex flex-row pt-2 cursor-pointer border-b">
            <UserCircleIcon className="h-12 mr-2 mb-2 text-bongasandbox-blue fill-current" />
            <div className="flex flex-col w-5/6">
                <div className="flex justify-between items-center text-gray-800">
                    <span className="text-base font-interBold">{title}</span>
                    <span className="text-sm text-gray-400 inline-flex items-center">
                        {time} &nbsp;
                        <ChevronRightIcon className="w-3 h-3 text-gray-400" />
                    </span>
                </div>
                <p className=" text-xs font-interSemiBold text-gray-400 truncate">
                {description}</p>
            </div>
        </Link>

    )

}

export default ChatListItem;