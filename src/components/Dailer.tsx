import { BackspaceIcon, PhoneIcon } from "@heroicons/react/24/solid";
import { DailpadValues } from "../constants/Properties";

interface IDailerProps {
	setValue: (value: string) => void;
	value: string;
	handleErase: () => void;
	handleCall: () => void;
}

const Dailer = (props: IDailerProps) => {
    

	const { value, setValue, handleErase, handleCall } = props;

	return (
		<div>
			<input type="text" name="value" id="value" value={value} autoFocus className="w-full text-2xl text-gray-800 text-center border-0 focus:ring-0"/>
			<div className="grid grid-cols-3 gap-3 grid-rows-3 py-4">
				{DailpadValues.map((value) => (
					<div className="flex justify-center items-center" onClick={() => setValue(value.item)}>
						<div className="flex flex-col justify-center items-center w-16 h-16 rounded-full p-3 cursor-pointer bg-gray-200">
							<>
								<span className="text-3xl text-gray-800">{value.item}</span>
								<span className="text-[10px] text-gray-700 uppercase">
                                    {value.subItems.map(subItem => subItem.item)}
								</span>
							</>
						</div>
					</div>
				))}
                <div className="flex justify-center items-center">
                </div>
                <div className="flex justify-center items-center">
                    <div className="flex flex-col justify-center items-center w-16 h-16 rounded-full p-3 cursor-pointer bg-green-500">
                        <PhoneIcon className="w-6 h-6 text-white -rotate-2" onClick={() => handleCall()}/>
                    </div>
                </div>
                <div className="flex justify-center items-center">
                        <BackspaceIcon className="w-8 h-8 text-gray-200" onClick={() => handleErase()}/>
                </div>
			</div>
		</div>
	);
};

export default Dailer;
