
interface IChatBubble {
    start?: boolean
    end?: boolean
    children: React.ReactNode
}

const ChatBubble = (props: IChatBubble) => {
    
    const { start, children } = props

    const chatBubbleContainerStyle = start === true ? 'pr-6 justify-start' : ' pl-16 justify-end'
    const chatBubbleWrapperStyle = start === true ? 'bg-gray-200 rounded-bl-none' : 'bg-bongasandbox-blue text-center text-white min-w-[3rem] rounded-br-none'

    return (
        <div className={`flex ${chatBubbleContainerStyle} `}>
            <div className="py-2 rounded-lg justify-end">
                <div className={`py-2 px-4 rounded-3xl ${chatBubbleWrapperStyle}`}><p>{children}</p></div>
            </div>
        </div>
    )
}

export default ChatBubble;