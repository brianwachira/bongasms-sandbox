import { Dialog, Transition } from "@headlessui/react";
import { CheckCircleIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { Fragment } from "react";

interface IModalInfo {
	title: string;
	description: string;
	modalVisible: boolean;
	toggleModal: () => void;
}
const ModalInfo = (props: IModalInfo) => {
	const { title, description, modalVisible, toggleModal } = props;

	return (
		<Transition appear show={modalVisible} as={Fragment}>
			<Dialog as="div" className="relative z-10" onClose={toggleModal}>
				<Transition.Child
					as={Fragment}
					enter="ease-out duration-300"
					enterFrom="opacity-0"
					enterTo="opacity-100"
					leave="ease-in duration-200"
					leaveFrom="opacity-100"
					leaveTo="opacity-0"
				>
					<div className="fixed inset-0 bg-bongasandbox-modal-overlay bg-opacity-70 backdrop-blur" />
				</Transition.Child>

				<div className="fixed inset-0 overflow-y-auto">
					<div className="flex min-h-full items-center justify-center p-4 text-center">
						<Transition.Child
							as={Fragment}
							enter="ease-out duration-300"
							enterFrom="opacity-0 scale-95"
							enterTo="opacity-100 scale-100"
							leave="ease-in duration-200"
							leaveFrom="opacity-100 scale-100"
							leaveTo="opacity-0 scale-95"
						>
							<Dialog.Panel className="w-full max-w-[400px] transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
								<div className="flex w-full justify-between items-center">
									<div className="w-12 h-12 rounded-3xl bg-green-50 cursor-pointer flex items-center justify-center">
										<div className="w-8 h-8 rounded-2xl bg-green-100 flex items-center justify-center">
											<CheckCircleIcon className="w-6 h-6 text-green-500" />
										</div>
									</div>
									<XMarkIcon
										className="w-6 h-6 cursor-pointer text-gray-500"
										onClick={toggleModal}
									/>
								</div>
								<Dialog.Title
									as="h3"
									className="text-lg text-bongasandbox-black font-interSemiBold"
								>
									{title}
								</Dialog.Title>

								<Dialog.Description
									className={
										"text-bongasandbox-grey-label text-sm font-interRegular"
									}
								>
									{description}
								</Dialog.Description>
								<div className=" my-5" />
								<button
									type="submit"
									className="w-full flex justify-center py-3 px-4 border border-transparent rounded-lg shadow-sm text-base font-interSemiBold text-white bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:outline-none"
									onClick={toggleModal}
								>
									Done
								</button>
							</Dialog.Panel>
						</Transition.Child>
					</div>
				</div>
			</Dialog>
		</Transition>
	);
};

export default ModalInfo