import {
	ArrowRightCircleIcon,
	ChatBubbleBottomCenterTextIcon,
} from "@heroicons/react/24/outline";
import { Link } from "react-router-dom";

interface ApiLinkCardProps {
	title: string;
	content: string;
	href: string;
}
const ApiLinkCard = (props: ApiLinkCardProps) => (
	<div
		className=" bg-bongasandbox-card-bg hover:bg-bongasandbox-card-bg-hover p-5 rounded-3xl transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-200"
		key={props.href}
	>
		<div className=" bg-bongasandboc-icon-overlay w-12 h-12 mb-5 flex items-center rounded-3xl">
			<ChatBubbleBottomCenterTextIcon className=" mx-auto w-6 h-6 text-bongasandbox-blue group-hover:text-bongasandbox-blue-hover" />
		</div>
		<div className="mb-5">
			<h5 className="text-xl font-interSemiBold text-gray-900">
				{props.title}
			</h5>
			<p className="text-base font-interRegular text-gray-600">
				{props.content}
			</p>
		</div>
		<a href={props.href} target="_blank" rel="noopener noreferrer"  className="group inline-flex items-center">
			Learn More{" "}
			<ArrowRightCircleIcon className=" ml-3 w-6 h-6 text-bongasandbox-blue group-hover:text-bongasandbox-blue-hover" />{" "}
		</a>
	</div>
);

export default ApiLinkCard;
