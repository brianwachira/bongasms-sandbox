

const PageNotFound = () => {

    return (
        <main>
            <p>404, sorry &#129394;, Page not found</p>
        </main>
    )
}

export default PageNotFound