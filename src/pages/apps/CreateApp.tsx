import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import { XMarkIcon } from "@heroicons/react/24/outline";
import ModalInfo from "../../components/shared-ui/ModalInfo";
import { useNavigate } from "react-router-dom";

const CreateApp = () => {
	const [isCreateAppModalOpen, setIsCreateAppModalOpen] = useState(true);
	const [isSuccessfulModalOpen, setSuccessfulModalOpen] = useState(false);

	const navigate = useNavigate();

	function closeCreateAppModal() {
		setIsCreateAppModalOpen(false);
		navigate("/apps");
	}

	function openCreateAppModal() {
		setIsCreateAppModalOpen(true);
	}

	function openSuccessModal() {
		setSuccessfulModalOpen(true);
	}
	function closeSuccessModal() {
		setSuccessfulModalOpen(false);
		navigate("/apps?createdapp=true");
	}

	const handleCreateApp = (event: React.ChangeEvent<HTMLFormElement>) => {
		event.preventDefault();
		closeCreateAppModal();
		openSuccessModal();
	};

	const handleSuccessModal = () => {
		closeSuccessModal();
	};

    const appOptions = [
        {
            service: 'SMS',
            description: 'Send quick SMS.'
        },
        {
            service: 'USSD',
            description: 'For all your USSD needs.'
        },
        {
            service: 'Premium SMS',
            description: 'Send SMS via shortcodes.'
        },
        {
            service: 'Utilities',
            description: 'Airtime, internet bundles.'
        }
    ]
    
	return (
		<>
			<Transition appear show={isCreateAppModalOpen} as={Fragment}>
				<Dialog
					as="div"
					className="relative z-10"
					onClose={closeCreateAppModal}
				>
					<Transition.Child
						as={Fragment}
						enter="ease-out duration-300"
						enterFrom="opacity-0"
						enterTo="opacity-100"
						leave="ease-in duration-200"
						leaveFrom="opacity-100"
						leaveTo="opacity-0"
					>
						<div className="fixed inset-0 bg-bongasandbox-modal-overlay bg-opacity-70 backdrop-blur" />
					</Transition.Child>

					<div className="fixed inset-0 overflow-y-auto">
						<div className="flex min-h-full items-center justify-center p-4 text-center">
							<Transition.Child
								as={Fragment}
								enter="ease-out duration-300"
								enterFrom="opacity-0 scale-95"
								enterTo="opacity-100 scale-100"
								leave="ease-in duration-200"
								leaveFrom="opacity-100 scale-100"
								leaveTo="opacity-0 scale-95"
							>
								<Dialog.Panel className="w-full max-w-sm transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
									<div className="flex w-full justify-between items-center">
										<Dialog.Title
											as="h3"
											className="text-lg text-bongasandbox-black font-interSemiBold"
										>
											Create App
										</Dialog.Title>

										<XMarkIcon
											className="w-6 h-6 cursor-pointer text-gray-500"
											onClick={closeCreateAppModal}
										/>
									</div>

									<Dialog.Description
										className={
											"text-bongasandbox-grey-label text-sm font-interRegular"
										}
									>
										Enter details to create an app
									</Dialog.Description>
									<div className=" my-5" />
									<form className="space-y-6" onSubmit={handleCreateApp}>
										<div className="mb-5">
											<label className="text-base text-gray-700 font-interMedium">
												App Name
											</label>
											<input
												id="appname"
												name="appname"
												type={"text"}
												autoComplete={"name"}
												className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
												placeholder="Enter Your App Name"
												required
											/>
										</div>
										<label className="text-base font-interMedium text-gray-700 mb-5">
											Which service(s) should be mapped to this app?
										</label>
                                        {appOptions.map(option =>
                                            <div className="flex" key={option.service+option.description}>
                                                <div className="flex items-center h-5">
                                                    <input
                                                        id="helper-checkbox"
                                                        aria-describedby="helper-checkbox-text"
                                                        type="checkbox"
                                                        value=""
                                                        className="w-5 h-5 text-bongasandbox-blue bongasandbox-tally-overlay rounded-md border-gray-300 border-2 focus:ring-bongasandbox-blue focus:ring-2 accent-bongasandbox-tally-overlay"
                                                    />
                                                </div>
                                                <div className="ml-2 text-sm">
                                                    <label
                                                        htmlFor="helper-checkbox"
                                                        className="text-base text-gray-700 font-interMedium"
                                                    >
                                                        {option.service}
                                                    </label>
                                                    <p
                                                        id="helper-checkbox-text"
                                                        className="text-base font-interRegular text-gray-600"
                                                    >
                                                        {option.description}
                                                    </p>
                                                </div>
                                            </div>
                                        )}
										<button
											type="submit"
											className="w-full flex justify-center py-3 px-4 mb-8 border border-transparent rounded-lg shadow-sm text-base font-interSemiBold text-white bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:outline-none"
										>
											Create
										</button>
									</form>
								</Dialog.Panel>
							</Transition.Child>
						</div>
					</div>
				</Dialog>
			</Transition>
			<ModalInfo
				title="Account Created Successfully"
				description="Thanks for contacting us! We will be in touch with you shortly"
				modalVisible={isSuccessfulModalOpen}
				toggleModal={handleSuccessModal}
			/>
		</>
	);
};

export default CreateApp;
