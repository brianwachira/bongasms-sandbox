import {
	ArrowSmallRightIcon,
	EyeIcon,
	PlusIcon,
} from "@heroicons/react/24/outline";
import { Link, Outlet, useLocation } from "react-router-dom";
import LayoutLoggedIn from "../../layouts/LayoutLoggedIn";
import { ReactComponent as CloudEmpty } from "../../assets/icons/cloud-empty.svg";
import { useEffect, useState } from "react";
import apps from "../../mock/apps.json";
import { ReactComponent as AppsBannerImage } from "../../assets/images/appsbannerImage2.svg";

let appsMock = apps;

const Apps = () => {
	const params = useLocation();
	console.log(params.search);

	const [apps, setApps] = useState<typeof appsMock>([]);

	useEffect(() => {
		if (params.search.length > 0) {
			setApps(appsMock);
		}
	}, [params.search]);

	return (
		<LayoutLoggedIn>
			<section className="xl:w-[90vw] space-y-1 px-5 mx-auto  grid min-h-[calc(100vh_-_82px)]">
				{/* <div className="grid self-start" /> */}
				{apps?.length < 1 ? (
					<>
						<div className="grid self-center text-center">
							<Link
								to={"#"}
								className="mx-auto mt-8 lg:mt-0 mb-8 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-200"
							>
								<div className=" bg-bongasandbox-tally-overlay flex items-center p-1 rounded-2xl">
									<span className=" w-8 h-6 rounded-2xl bg-white text-bongasandbox-blue mr-3 text-center">
										0
									</span>
									<span className=" font-interMedium text-bongasandbox-blue text-sm mr-3">
										Sandbox app
									</span>
									<ArrowSmallRightIcon className=" text-bongasandbox-blue w-4 h-4" />
								</div>
							</Link>
							<h1 className=" font-interSemiBold text-3xl lg:text-6xl text-gray-900 text-center mb-6">
								Welcome Spencer Owino
							</h1>
							<p className=" font-interRegular text-xl text-gray-600 text-center w-3/5 mx-auto mb-7">
								All your apps appear here. Sandbox apps are marked grey,
								production apps are marked blue while revoked apps are marked
								red
							</p>
							<div>
								<CloudEmpty className="absolute mx-auto animate-ping opacity-90 left-0 right-0" />
								<CloudEmpty className="relative mx-auto" />
							</div>
							<h4 className="my-3 text-gray-900 font-interSemiBold text-lg mx-auto">
								No projects found!
							</h4>
							<p className=" text-gray-600 font-interRegular text-sm w-1/2 lg:w-1/4 mx-auto mb-8">
								Hmm... looks like you do not have any apps curently.Create an
								app
							</p>
							<Link to="/apps/createapp" className="mx-auto">
								<div className="mb-5 inline-flex items-center px-20 py-2 border border-transparent text-md font-interBold rounded-lg shadow-sm text-white bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-bongasandbox-black">
									+ Create New App
								</div>
							</Link>
						</div>
					</>
				) : (
					<>
						<div className="grid self-center">
							<Link
								to={"#"}
								className="mx-auto mb-8 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-200"
							>
								<div className=" bg-bongasandbox-tally-overlay flex items-center p-1 rounded-2xl mt-4">
									<span className=" w-8 h-6 rounded-2xl bg-white text-bongasandbox-blue mr-3 text-center">
										{apps.length}
									</span>
									<span className=" font-interMedium text-bongasandbox-blue text-sm mr-3">
										Sandbox app
									</span>
									<ArrowSmallRightIcon className=" text-bongasandbox-blue w-4 h-4" />
								</div>
							</Link>
							<h1 className=" font-interSemiBold text-3xl lg:text-6xl text-gray-900 text-center mb-6">
								Welcome Spencer Owino
							</h1>
							<p className=" font-interRegular text-xl text-gray-600 text-center  w-3/5 mx-auto mb-7">
								All your apps appear here. Sandbox apps are marked grey,
								production apps are marked blue while revoked apps are marked
								red
							</p>
							<Link to="/apps/createapp" className="mx-auto">
								<div className="mb-5 group inline-flex items-center px-20 py-2 border border-transparent text-md font-interBold rounded-lg shadow-sm text-white bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-bongasandbox-black">
									<PlusIcon className=" w-5 h-5 text-white transition delay-150 group-hover:rotate-180" />{" "}
									Create New App
								</div>
							</Link>
							<label
								htmlFor="default-search"
								className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
							>
								Search
							</label>
							<div className="relative w-[83vw] xl:w-[63vw] mx-auto mt-6 mb-12">
								<div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
									<svg
										aria-hidden="true"
										className="w-5 h-5 text-gray-500 dark:text-gray-400"
										fill="none"
										stroke="currentColor"
										viewBox="0 0 24 24"
										xmlns="http://www.w3.org/2000/svg"
									>
										<path
											stroke-linecap="round"
											stroke-linejoin="round"
											stroke-width="2"
											d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
										></path>
									</svg>
								</div>
								<input
									type="search"
									id="default-search"
									className="block w-full h-12	 p-3 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-bongasandbox-blue-hover focus:border-bongasandbox-blue-hover"
									placeholder="Search Apps"
									required
								/>
								<button
									type="submit"
									className="h-12 text-white text-md font-interSemiBold font-semibold  absolute right-0 bottom-0 bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:ring-4 focus:outline-none focus:ring-bongasandbox-blue rounded-tr-lg rounded-br-lg px-8 py-3"
								>
									Search
								</button>
							</div>
							<div className="group xl:w-[83vw] mx-auto grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6 mb-7 px-5 lg:px-0">
								{apps.map((app) => (
									<div className=" bg-white shadow-xl rounded-b-xl">
										<AppsBannerImage className=" rounded-t-xl" />
										<div className="p-6">
											<div className="flex justify-between items-center">
												<h4 className=" font-interSemiBold text-2xl text-gray-900">
													{app.name}
												</h4>
												<div className="flex items-center">
													<span className="flex h-3 w-3 mr-2">
														<span className="animate-ping absolute inline-flex h-3 w-3 rounded-full bg-green-400 opacity-90"></span>
														<span className="relative inline-flex rounded-full h-3 w-3 bg-green-500"></span>
													</span>
													<EyeIcon className=" w-6 h-6 text-gray-900" />
												</div>
											</div>
											<div className="inline-flex">
												<p className="font-interSemiBold text-base text-gray-600">
													Domain(s) : &nbsp;
												</p>
												<span className=" font-interRegular text-base text-gray-600">
													{" "}
													{app.service.map((item) => " " + item.name + " | ")}
												</span>
											</div>
											<div className=" mt-6">
												<div className="mb-6">
													<h5 className="font-interMedium text-sm text-gray-700">
														Consumer Client ID
													</h5>
													<p className=" font-interRegular text-sm text-gray-600">
														{app.clientId}
													</p>
												</div>
												<div className="mb-6">
													<h5 className="font-interMedium text-sm text-gray-700">
														Consumer Key
													</h5>
													<p className=" font-interRegular text-sm text-gray-600">
														{app.apikey}
													</p>
												</div>
												<div className="mb-6">
													<h5 className="font-interMedium text-sm text-gray-700">
														Consumer Secret
													</h5>
													<p className=" font-interRegular text-sm text-gray-600">
														{app.secret}
													</p>
												</div>
												<h5 className="font-interMedium text-sm text-gray-700">
													Created On
												</h5>
												<p className=" font-interRegular text-sm text-gray-600">
													{app.created_at}
												</p>
											</div>
										</div>
									</div>
								))}
							</div>
						</div>
					</>
				)}
				{/* <div className="grid self-end" /> */}
			</section>
			<Outlet />
		</LayoutLoggedIn>
	);
};

export default Apps;
