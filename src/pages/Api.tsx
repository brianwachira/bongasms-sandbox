import LayoutLoggedIn from "../layouts/LayoutLoggedIn";
import LinePattern from "../assets/images/LinePattern.png";
import { ApiLinks } from "../constants/Properties";
import { Link } from "react-router-dom";
import {
	ArrowRightCircleIcon,
	ChatBubbleBottomCenterTextIcon,
} from "@heroicons/react/24/outline";
import LayoutDefault from "../layouts/LayoutDefault";
import ApiLinkCard from "../components/ApiLinkCard";

const Api = () => {
	return (
		<LayoutDefault>
			<section className=" bg-bongasandbox-blue relative">
				<img
					src={LinePattern}
					alt="lineContainer"
					className="absolute inset-0 h-full w-full object-cover"
				/>
				<main className="xl:w-[83vw] py-24 mx-auto px-5 lg:px-0">
					<p className="text-base font-interSemiBold text-white text-center lg:text-left ">
						Nice to meet you
					</p>
					<div className=" flex flex-col lg:flex-row justify-between items-baseline">
						<h1 className="mt-4 text-3xl xl:text-5xl text-white mb-1 font-interSemiBold text-center lg:text-left">
							Welcome Spencer Owino
						</h1>
						<p className=" text-xl font-interRegular text-white lg:w-96 text-center lg:text-left">
							All your API appear here. Sandbox apps are marked grey, production
							apps are marked blue while revoked apps are marked red
						</p>
					</div>
				</main>
			</section>
			<section>
				<main className="xl:w-[83vw]  pt-16 pb-8 mx-auto px-5 lg:px-0">
					<h1 className=" font-interSemiBold text-4xl text-gray-900 mb-5">
						Start doing work that matters.
					</h1>
					<p className=" font-interRegular text-xl text-gray-600  lg:w-3/5">
						An application programming interface (API) is a way for two or more
						computer programs to communicate with each other.
					</p>
				</main>
				<div className="group xl:w-[83vw] mx-auto grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-8 mb-7 px-5 lg:px-0">
					{ApiLinks.map((item) => (
						<ApiLinkCard
							title={item.title}
							content={item.content}
							href={item.href}
						/>
					))}
				</div>
			</section>
		</LayoutDefault>
	);
};

export default Api;
