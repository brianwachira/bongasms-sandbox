import { Link } from "react-router-dom";
import LayoutDefault from "../layouts/LayoutDefault";
import { ReactComponent as BannerImage } from '../assets/images/Container.svg';
import { PlusIcon } from "@heroicons/react/24/outline";

const LandingPage = () => {
	return (
		<LayoutDefault>
			<section className="xl:w-[90vw] space-y-1 px-5 mx-auto  grid h-[calc(100vh_-_82px)]">
                <div className='hidden md:grid self-start'/>
                <div className="grid self-center text-center">
					<h1 className="mt-4 text-3xl xl:text-6xl xl:leading-[72px] text-gray-900 font-interMedium">
						Harness the power of
					</h1>
					<h1 className=" text-3xl xl:text-6xl xl:leading-[72px] text-bongasandbox-blue hover:text-bongasandbox-blue-hover font-interBold cursor-pointer  mb-5">
						mobile messaging
					</h1>
					<div className=" w-3/4 sm:w-6/12 mx-auto mb-5">
						<p className=" text-gray-600 text-xl font-interRegular text-center">
							—{" "}
							<span className="text-xl text-gray-900 hover:text-bongasandbox-blue-hover font-interSemiBold cursor-pointer">
								BongaSMS
							</span>{" "}
							is a powerful communication platform that is easy to use and
							guaranteed to meet even your most demanding messaging needs.{" "}
						</p>
					</div>

					<Link to="/auth">
						<div className="group mb-5 ml-6 inline-flex items-center px-16 py-2 border border-transparent text-md font-semibold rounded-lg shadow-sm text-white bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-bongasandbox-black">
							<PlusIcon className=" w-5 h-5 text-white transition delay-150 group-hover:rotate-180"/> Create New App
						</div>
					</Link>
				</div>
                <div className="hidden md:grid self-end ">
                    <BannerImage className="mx-auto"/>
                </div>

			</section>
		</LayoutDefault>
	);
};

export default LandingPage;
