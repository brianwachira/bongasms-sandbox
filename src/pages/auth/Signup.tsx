import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import { XMarkIcon } from "@heroicons/react/24/outline";
import ModalInfo from "../../components/shared-ui/ModalInfo";
import { useNavigate } from "react-router-dom";

const Signup = () => {
	const [isSignUpModalOpen, setIsSignUpModalOpen] = useState(true);
	const [isSuccessfulModalOpen, setSuccessfulModalOpen] = useState(false);

	const navigate = useNavigate();

	function closeSignupModal() {
		setIsSignUpModalOpen(false);
		navigate(-1);
	}

	function openSignupModal() {
		setIsSignUpModalOpen(true);
	}

	function openSuccessModal() {
		setSuccessfulModalOpen(true);
	}
	function closeSuccessModal() {
		setSuccessfulModalOpen(false);
	}

	const handleSignup = (event: React.ChangeEvent<HTMLFormElement>) => {
		event.preventDefault();
		setIsSignUpModalOpen(false);
		openSuccessModal();
	};

	const handleSuccessModal = () => {
		closeSuccessModal();
		navigate(-1);
	};

	return (
		<>
			<Transition appear show={isSignUpModalOpen} as={Fragment}>
				<Dialog as="div" className="relative z-10" onClose={closeSignupModal}>
					<Transition.Child
						as={Fragment}
						enter="ease-out duration-300"
						enterFrom="opacity-0"
						enterTo="opacity-100"
						leave="ease-in duration-200"
						leaveFrom="opacity-100"
						leaveTo="opacity-0"
					>
						<div className="fixed inset-0 bg-bongasandbox-modal-overlay bg-opacity-70 backdrop-blur" />
					</Transition.Child>

					<div className="fixed inset-0 overflow-y-auto">
						<div className="flex min-h-full items-center justify-center p-4 text-center">
							<Transition.Child
								as={Fragment}
								enter="ease-out duration-300"
								enterFrom="opacity-0 scale-95"
								enterTo="opacity-100 scale-100"
								leave="ease-in duration-200"
								leaveFrom="opacity-100 scale-100"
								leaveTo="opacity-0 scale-95"
							>
								<Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
									<div className="flex w-full justify-between items-center">
										<Dialog.Title
											as="h3"
											className="text-lg text-bongasandbox-black font-interSemiBold"
										>
											Sign up
										</Dialog.Title>

										<XMarkIcon
											className="w-6 h-6 cursor-pointer text-gray-500"
											onClick={closeSignupModal}
										/>
									</div>

									<Dialog.Description
										className={
											"text-bongasandbox-grey-label text-sm font-interRegular"
										}
									>
										Enter details to sign up
									</Dialog.Description>
									<div className=" my-5" />
									<form className="space-y-6" onSubmit={handleSignup} autoComplete="on">
										<div className="grid grid-cols-2 gap-x-4 ">
                                        <label className="text-sm text-gray-700 font-interMedium col-span-2">
											Names*
										</label>
											<div>
												<label className="text-sm text-gray-700 font-interMedium">
													First name*
												</label>
												<input
													id="firstname"
													name="firstname"
													type={"text"}
													className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
													placeholder="Enter Your first name"
													value={"Kai"}
												/>
											</div>
											<div>
												<label className="text-sm text-gray-700 font-interMedium">
													Last name*
												</label>
												<input
													id="lastname"
													name="lastname"
													type={"text"}
													className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
													placeholder="Enter Your last name"
													value={"blue"}
												/>
											</div>
										</div>
                                        <div>
                                            <label className="text-sm text-gray-700 font-interMedium">
                                                Email*
                                            </label>
                                            <input
                                                id="email"
                                                name="email"
                                                type={"email"}
                                                autoComplete={"email"}
                                                className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
                                                placeholder="Enter Your last email"
                                                value={"kaiblue@gmail.com"}
                                            />
                                        </div>
										<div>
											<label className="text-sm text-gray-700 font-interMedium">
												Password*
											</label>
											<input
												id="password"
												name="password"
												type={"password"}
												autoComplete={"password"}
												className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
												placeholder="Enter Your Password"
												value={"spencerowino@gmail.com"}
											/>
										</div>
										<div>
											<label className="text-sm text-gray-700 font-interMedium">
												Confirm Password*
											</label>
											<input
												id="confirmpassword"
												name="confirmPassword"
												type={"password"}
												className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
												placeholder="Confirm Your Password"
												value={"spencerowino@gmail.com"}
											/>
										</div>
										<button
											type="submit"
											className="w-full flex justify-center py-3 px-4 mb-8 border border-transparent rounded-lg shadow-sm text-base font-interSemiBold text-white bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:outline-none"
										>
											Sign up
										</button>
										<div className="w-full inline-flex justify-center">
											<p className="mr-2 text-sm font-interRegular">
												Have an account?
											</p>{" "}
											<a href="/auth/" className="text-sm font-interSemiBold">
												Sign in
											</a>
										</div>
									</form>
								</Dialog.Panel>
							</Transition.Child>
						</div>
					</div>
				</Dialog>
			</Transition>
			<ModalInfo
				title="Account Created Successfully"
				description="Thanks for contacting us! We will be in touch with you shortly"
				modalVisible={isSuccessfulModalOpen}
				toggleModal={handleSuccessModal}
			/>
		</>
	);
};

export default Signup;
