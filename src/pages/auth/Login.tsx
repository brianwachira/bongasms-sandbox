import { Outlet, useNavigate } from 'react-router-dom'
import { ReactComponent as BongaIconLight} from '../../assets/icons/bonga-icon-light.svg'
import { ReactComponent as StarsIcon } from '../../assets/icons/Stars.svg'
import BannerImage from '../../assets/images/Rectangle87.png'

const Login = () => {

    const navigate =  useNavigate();

    const handleLogin = (event :  React.ChangeEvent<HTMLFormElement>) => {
        event.preventDefault()
        navigate('/apps')
    }
    return (
        <main>
            <div className="min-h-screen bg-white flex">
                <div className="basis-1/2 flex-1 flex flex-col justify-center py-12 px-4 sm:px-6  lg:px-20 xl:px-24">
                    <div className="mx-auto lg:w-96 max-w-sm grid h-[calc(100vh_-_120px)]">
                        <div className='grid self-start'/>
                        <div className='grid self-center'>
                            <BongaIconLight className='mb-12'/>
                            <div className='my-8'>
                                <h2 className='mb-3 text-bongasandbox-black text-4xl font-interSemiBold'>Log in</h2>
                                <p className='text-base text-gray-600 font-interRegular'>Welcome! Please enter your details.</p>
                            </div>
                            <form className='space-y-6' onSubmit={handleLogin}>
                                <div>
                                    <label className='text-sm text-gray-700 font-interMedium'>Email</label>
                                    <input 
                                    id="email" 
                                    name="email" 
                                    type={'email'} 
                                    autoComplete={'email'}
                                    className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
                                    placeholder='Enter Your Email'
                                    value={'spencerowino@gmail.com'}/>
                                </div>
                                <div>
                                    <label className='text-sm text-gray-700 font-interMedium'>Password</label>
                                    <input 
                                    id="password" 
                                    name="password" 
                                    type={'password'} 
                                    autoComplete={'password'}
                                    className="appearance-none block w-full mt-2 px-3 py-3 border border-gray-300 rounded-lg shadow-sm text-gray-500 placeholder-gray-500 placeholder:text-base placeholder:font-interRegular focus:outline-none focus:ring-bongasandbox-blue focus:border-bongasandbox-blue-hover sm:text-sm"
                                    placeholder='Enter Your Password'
                                    value={'spencerowino@gmail.com'}
                                    />
                                </div>
                                <div className='flex justify-center'>
                                    <a href='/auth/forgotpassword' className='text-sm font-interSemiBold'>Forgot Password</a>
                                </div>
                                <button type="submit" className="w-full flex justify-center py-3 px-4 mb-8 border border-transparent rounded-lg shadow-sm text-base font-interSemiBold text-white bg-bongasandbox-blue hover:bg-bongasandbox-blue-hover focus:outline-none">Sign in</button>
                                <div className='w-full inline-flex justify-center'>
                                    <p className='mr-2 text-sm font-interRegular'>Don't have an account?</p>  <a href='/auth/signup' className='text-sm font-interSemiBold'>Sign up</a>
                                </div>
                            </form>
                        </div>
                        <div className="grid self-end"/>
                    </div>
                </div>
                <div className="basis-1/2 hidden lg:block relative w-0 flex-1">
                    <img src={BannerImage} alt="banner" className="absolute inset-0 h-full w-full object-cover" />
                    <div aria-hidden="true" className="absolute h-full w-full mix-blend-multiply bg-gradient-to-b from-bongasandbox-blue to-bongasandbox-grey-overlay opacity-100"/>
                    <div className=" p-28 z-10 grid h-screen">
                        <div className="grid self-center">
                            <StarsIcon className='z-10 mb-12 animate-pulse'/>
                            <p className=" text-7xl text-white font-interMedium z-10 mb-6 max-w-md">Harness the power of mobile messaging</p>
                            <p className=" text-xl text-white font-interMedium z-10 max-w-md">BongaSMS is a powerful communication platform that that is easy to use and guaranteed to meet even your most demanding messaging needs.</p>
                        </div>
                        <div className="grid self-end"/>
                    </div>
                </div>
                <Outlet/>
            </div>
        </main>
    )
}

export default Login