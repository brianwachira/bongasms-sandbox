import LayoutLoggedIn from "../../layouts/LayoutLoggedIn";
import HomeScreen from "./HomeScreen";
import DailScreen from "./DailScreen";
import { useLocation } from "react-router-dom";
import ChatScreen from "./ChatScreen";
import CreateChatScreen from "./CreateChatScreen";

const Simulator = () => {
	
	const params = useLocation();
	return (
		<LayoutLoggedIn>
			<section className="xl:w-[85vw] space-y-1 px-5 mx-auto  grid min-h-[calc(100vh_-_82px)]">
				<div className="grid self-center">
					<RenderSimulator currentScreen={params.search}/>
				</div>
			</section>
		</LayoutLoggedIn>
	);
};

const RenderSimulator = ({ currentScreen} : { currentScreen : '?home' | '?dail' | '?chat' | string }) : JSX.Element => {
		console.log(currentScreen)
	if (currentScreen=== '?dial') {
		return <DailScreen/>
	} else if (currentScreen === '?chat') {
		return <ChatScreen/>
	} else if(currentScreen == '?createmessage') {
		return <CreateChatScreen/>
	}
 
	return <HomeScreen/>
}
export default Simulator;
