import { useState } from "react"
import Dailer from "../../components/Dailer"
import Simulator from "../../components/simulator/Simulator"

const DailScreen = () => {
    const [dailpadValue, setDailpadValue] = useState<string>('')

	const setValues = (value: string) => {
		setDailpadValue(dailpadValue + value)
	}

	const eraseValue = () => {
		let newValue = dailpadValue.replace(dailpadValue.charAt(dailpadValue.length - 1),'');

		setDailpadValue(newValue)
	}

	const callNumber = () => {
		alert("Coming soon!")
	}

    return (
        <Simulator>
            <Simulator.Description>
                <Simulator.Description.Heading>Simulate USSD interactions</Simulator.Description.Heading>
                <Simulator.Description.Content>Simulate USSD interactions for developers with our tool. Test and refine your USSD apps with our simulation platform.</Simulator.Description.Content>
            </Simulator.Description>
            <Simulator.Screen>
                <div className="mt-auto">
                    <Dailer
                        value={dailpadValue} 
                        setValue={setValues} 
                        handleErase={eraseValue}
                        handleCall={callNumber}/>
                </div>
            </Simulator.Screen>
        </Simulator>
    )

}

export default DailScreen;