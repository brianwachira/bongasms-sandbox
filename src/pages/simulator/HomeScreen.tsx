import Simulator from "../../components/simulator/Simulator"
import { ReactComponent as BongaIconLight } from '../../assets/icons/bonga-icon-light.svg'

const HomeScreen = () => {
    return(
        <Simulator>
            <Simulator.Description>
                <Simulator.Description.Heading>Message like a pro with our developer simulator.</Simulator.Description.Heading>
                <Simulator.Description.Content>Simplify your testing with our messaging and USSD simulation tool. Make your messaging and USSD services bulletproof with our developer simulator.</Simulator.Description.Content>
            </Simulator.Description>
            <Simulator.Screen>
                <div className="my-24">
                    <BongaIconLight className="w-28 mx-auto animate-pulse"/>
                </div>
            </Simulator.Screen>
        </Simulator>
    )
}

export default HomeScreen