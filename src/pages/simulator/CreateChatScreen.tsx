import { ChevronLeftIcon, PencilSquareIcon } from "@heroicons/react/24/outline";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import ChatBubble from "../../components/ChatBubble";
import Simulator from "../../components/simulator/Simulator";

const CreateChatScreen = () => {
	const [message, setMessage] = useState("");
	const [sendersThread, setSendersThread] = useState<string[]>([
		"hello",
		"hello",
		"hello",
	]);

	const handleSendersThread = (sendersMessage: string) => {
		setSendersThread([...sendersThread, sendersMessage]);
	};

	const submitMessage = () => {
		handleSendersThread(message);
	};

	const handleMessageChange = (event: React.ChangeEvent<HTMLTextAreaElement>) =>
		setMessage(event.target.value);

	useEffect(() => {
		const keyDownHandler = (event: KeyboardEvent) => {
			if (event.key === "Enter") {
				event.preventDefault();

				// submit message
				submitMessage();
			}
		};
		document.addEventListener("keydown", keyDownHandler);
		return () => {
			document.removeEventListener("keydown", keyDownHandler);
		};
	}, []);

	return (
		<Simulator>
			<Simulator.Description>
				<Simulator.Description.Heading>
					Simulate real-life conversations
				</Simulator.Description.Heading>
				<Simulator.Description.Content>
					Test and refine your messaging apps with our simulation platform.
					Discover and fix bugs before they impact your users with our
					simulation tool.
				</Simulator.Description.Content>
			</Simulator.Description>
			<Simulator.Screen>
				<div className="flex flex-col h-[88%] justify-between">
					{sendersThread.length < 1 ? (
						<div className="flex flex-col shadow-md -mx-3">
							<div className="flex flex-row text-gray-800 mx-2 items-center mt-3">
								<Link to="?chat">
									<ChevronLeftIcon className="w-5 h-5 text-gray-600" />
								</Link>
								<span className="text-base text-center pl-5">New message</span>
							</div>
							<div className="flex flex-row items-center text-gray-800 mb-2">
								<span className="pl-4 pr-3">To:</span>
								<input
									type="text"
									name=""
									id=""
									className="text-sm pl-0 w-full text-gray-800 border-0 focus:ring-0"
									placeholder="Type a shortcode"
									autoFocus
									required
								/>
							</div>
						</div>
					) : (
						<>
							<div className="flex flex-row text-gray-800 mx-2 items-center mt-3">
								<Link to="?chat">
									<ChevronLeftIcon className="w-5 h-5 text-gray-600" />
								</Link>
								<span className="text-base text-center pl-5">Shortcode</span>
							</div>
							<div className="flex flex-col mt-auto pt-4 overflow-hidden overflow-y-auto">
								<ChatBubble end>start</ChatBubble>
								<ChatBubble start>
									To continue in english, send 1. Kuendelea na Kiswahili, tuma 2
								</ChatBubble>
								<ChatBubble end>1</ChatBubble>
								<ChatBubble start>
									Welcome to Shortcode
									<br />
									1. Internet Bundles.
									<br />
									2. Hustler Fund.
									<br />
								</ChatBubble>
								<ChatBubble end>2</ChatBubble>
								<ChatBubble start>
									To access startup fund, dial the USSD code *254#
									<br />
									Thank you
								</ChatBubble>
								<ChatBubble end>STOP</ChatBubble>
								<ChatBubble start>
									You have unsubscribed from Shortcode.
									<br />
									Thank you
								</ChatBubble>
							</div>
						</>
					)}
					<form className=" px-1 py-2">
						<textarea
							id="message"
							name="message"
							value={message}
							onChange={handleMessageChange}
							placeholder="Message"
							className="text-base appearance-none block text-gray-800 w-full rounded-lg border-gray-400 p-3 resize-none leading-tight focus:ring-gray-400 focus:border-gray-400 "
						></textarea>
					</form>
				</div>
			</Simulator.Screen>
		</Simulator>
	);
};

export default CreateChatScreen;
