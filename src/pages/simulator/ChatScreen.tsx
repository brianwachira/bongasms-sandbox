import {
	PencilSquareIcon,
} from "@heroicons/react/24/outline";
import { Link } from "react-router-dom";
import ChatListItem from "../../components/ChatListItem";
import Simulator from "../../components/simulator/Simulator";
import chatlist from "../../mock/chatlist.json";
const ChatScreen = () => {
	return (
		<Simulator>
			<Simulator.Description>
				<Simulator.Description.Heading>
					Simulate real-life conversations
				</Simulator.Description.Heading>
				<Simulator.Description.Content>
					Test and refine your messaging apps with our simulation platform.
					Discover and fix bugs before they impact your users with our
					simulation tool.
				</Simulator.Description.Content>
			</Simulator.Description>
			<Simulator.Screen>
				<Link to={"?createmessage"}>
					<PencilSquareIcon className="w-6 h-6 text-gray-600 ml-auto mt-3" />
				</Link>
				<h3 className=" text-2xl font-interBold">Messages</h3>
				<div className="flex flex-col h-full overflow-y-scroll">
					{chatlist.map((chatListItem) => (
						<ChatListItem
							id={chatListItem.id}
							title={chatListItem.title}
							time={chatListItem.time}
							description={chatListItem.description}
						/>
					))}
				</div>
			</Simulator.Screen>
		</Simulator>
	);
};

export default ChatScreen;
