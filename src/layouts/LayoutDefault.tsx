import React from "react"
import NavBar from "../components/NavBar"

interface ILayoutDefaultProps {
    children: React.ReactNode
}
const LayoutDefault = ({children} : ILayoutDefaultProps) => {
    return(
        <>
            <NavBar/>
            {children}
        </>
    )
}

export default LayoutDefault