import NavBarLoggedIn from "../components/NavBarLoggedIn";

interface ILayoutLoggedInProps {
    children: React.ReactNode
}

const LayoutLoggedIn = ({children}: ILayoutLoggedInProps) => {

    return (
        <>
            <NavBarLoggedIn/>
            {children}
        </>
    )
}

export default LayoutLoggedIn;