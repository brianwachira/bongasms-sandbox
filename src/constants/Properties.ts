export const ApiLinks = [
    {
      title: 'Transfer Credits',
      content: 'Transfer credits from one account to another',
      href: "https://api.olivetreehub.com/credits/transfercredits",
    },
    {
      title: 'Check Balance',
      content: 'Check your balance',
      href: "https://api.olivetreehub.com/credits/checkbalance",
    },
    {
      title: 'Send Quick SMS',
      content: 'Send a quick SMS',
      href: "https://api.olivetreehub.com/sms/quicksms/sendsms",
    },
    {
      title: 'Send Bulk SMS',
      content: 'Send a bulk SMS',
      href: "https://api.olivetreehub.com/sms/quicksms/sendbulksms",
    },
    {
      title: 'Send Custom SMS',
      content: 'Send a custom SMS',
      href: "https://api.olivetreehub.com/sms/quicksms/sendcustomsms",
    },
    {
      title: 'Mobile Originating',
      content: 'Send a custom SMS',
      href: "https://api.olivetreehub.com/sms/premiumsms/mobileoriginating",
    },
    {
      title: 'Mobile Terminating',
      content: 'Send a custom SMS',
      href: "https://api.olivetreehub.com/sms/premiumsms/mobileterminating",
    },
    {
      title: 'Bulk Data',
      content: 'Purchase & manage bulk data bundles',
      href: "https://api.olivetreehub.com/bulkdata",
    },
    {
      title: 'USSD',
      content: 'Create USSD menus',
      href: "https://api.olivetreehub.com/ussd",
    },
    {
      title: 'Fetch Delivery Report',
      content: 'Get delivery reports',
      href: "https://api.olivetreehub.com/reports/fetchdelivery",
    },
    {
      title: 'Send Quick Email',
      content: 'Send a quick email',
      href: "https://api.olivetreehub.com/emails/quickemail",
    },
    {
      title: 'Send Bulk Email',
      content: 'Send bulk emails',
      href: "https://api.olivetreehub.com/emails/bulkemail",
    },
    {
      title: 'Airtime',
      content: 'Purchase & manage airtime bundles',
      href: "https://api.olivetreehub.com/airtime",
    },
    {
      title: 'Electricity Tokens',
      content: 'Purchase & manage electricity tokens',
      href: "https://api.olivetreehub.com/electricitytokens",
    },
    {
      title: 'Bonga na MPESA',
      content: 'Intergrate Bonga with MPESA',
      href: "https://api.olivetreehub.com/bonganampesa",
    },
    {
      title: 'Bulk Payments',
      content: 'Make bulk payments',
      href: "https://api.olivetreehub.com/bulkpayments",
    }

]

export const DailpadValues = [
  {
    item: "1",
    subItems: [],
  },
  {
    item: "2",
    subItems: [
      {
        item: "A",
      },
      {
        item: "B",
      },
      {
        item: "C",
      },
    ],
  },
  {
    item: "3",
    subItems: [
      {
        item: "D",
      },
      {
        item: "E",
      },
      {
        item: "F",
      },
    ],
  },
  {
    item: "4",
    subItems: [
      {
        item: "G",
      },
      {
        item: "H",
      },
      {
        item: "I",
      },
    ],
  },
  {
    item: "5",
    subItems: [
      {
        item: "J",
      },
      {
        item: "K",
      },
      {
        item: "L",
      },
    ],
  },
  {
    item: "6",
    subItems: [
      {
        item: "M",
      },
      {
        item: "N",
      },
      {
        item: "O",
      },
    ],
  },
  {
    item: "7",
    subItems: [
      {
        item: "P",
      },
      {
        item: "Q",
      },
      {
        item: "R",
      },
      {
        item: "S",
      },
    ],
  },
  {
    item: "8",
    subItems: [
      {
        item: "T",
      },
      {
        item: "U",
      },
      {
        item: "V",
      },
    ],
  },
  {
    item: "9",
    subItems: [
      {
        item: "W",
      },
      {
        item: "X",
      },
      {
        item: "Y",
      },
      {
        item: "Z",
      },
    ],
  },
  {
    item: "*",
    subItems: [],
  },
  {
    item: "0",
    subItems: [
      {
        item: "+",
      },
    ],
  },
  {
    item: "#",
    subItems: [],
  },
];